# Protopia Ecosystems Basic Module

Основной [модуль](https://protopiahome-public.gitlab.io/protopia-ecosystem/cra-template-pe/modules/) для [ProtopiaEcosystem React Client](https://protopiahome-public.gitlab.io/protopia-ecosystem/cra-template-pe/). Включает в себя набор Экранов, Виджетов, настроек и ассетов, без которых приложение скомпеллируется корректно.

Данный пакет автоматически устанавливается в **Create-react-app приложение**

