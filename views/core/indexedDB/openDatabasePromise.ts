
declare global {
  interface Window {
    mozIndexedDB?: any
    webkitIndexedDB?: any 
    webkitIDBTransaction?: any 
    msIDBTransaction?: any 
    webkitIDBKeyRange?: any 
    msIDBKeyRange?: any 
    msIndexedDB?: any 
  }
}

if ( !window.indexedDB ) window.indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB;
if ( !window.IDBTransaction ) window.IDBTransaction = window.webkitIDBTransaction || window.msIDBTransaction;
if ( !window.IDBKeyRange ) window.IDBKeyRange = window.webkitIDBKeyRange || window.msIDBKeyRange;
if ( !window.indexedDB ) throw new Error( 'IndexedDB is not awailable' );

const DB_NAME = 'objectStore';
const OBJECT_STORE_NAME = 'objectStore';

// Функция оборачивает обращение к indexedDB.open в Promise
function openDatabasePromise( keyPath: any )
{
  return new Promise( ( resolve, reject ) => 
  {
    const dbOpenRequest = window.indexedDB.open( DB_NAME, 1 );

    dbOpenRequest.onblocked = () => 
    {
      reject( 'Требуется обновление структуры базы данных, хранимой в вашем браузере, ' +
        'но браузер уведомил о блокировке базы данных.' );
    };

    dbOpenRequest.onerror = (err: any) => 
    {
      console.log( 'Unable to open indexedDB ' + DB_NAME );
      console.log( err );
      reject( 'Невозможно открыть базу данных, либо при её открытии произошла неисправимая ошибка.' +
       ( err.message ? 'Техническая информация: ' + err.message : '' ) );
    };

    dbOpenRequest.onupgradeneeded = (event:any) => 
    {
      const db = event.target ?  event.target.result : {};
      try {
        db.deleteObjectStore( OBJECT_STORE_NAME );
      } catch ( err ) { console.log( err ); }
      db.createObjectStore( OBJECT_STORE_NAME, { keyPath } );
    };

    dbOpenRequest.onsuccess = () => 
    {
      console.info( 'Successfully open indexedDB connection to ' + DB_NAME );
      resolve( dbOpenRequest.result );
    };

    dbOpenRequest.onerror = reject;
  } );
}

// Оборачиваем функции от ObjectStore, поддерживающие интерфейс IDBRequest
// в вызов с использованием Promise
function wrap( methodName: any  ) 
{
  return function() 
  {
    const [ objectStore, ...etc ] = arguments;
    return new Promise( ( resolve, reject ) => {
      const request = objectStore[ methodName ]( ...etc );
      request.onsuccess = () => resolve( request.result );
      request.onerror = reject;
    } );
  };
}
const deletePromise: any = wrap( 'delete' );
const getAllPromise: any = wrap( 'getAll' );
const getPromise: any = wrap( 'get' );
const putPromise: any = wrap( 'put' );

export default class IndexedDbRepository 
{

  dbConnection: any;
  error: any;
  keyPath: any;
  openDatabasePromise: any;

  constructor( keyPath: any ) 
  {
    this.error = null;
    this.keyPath = keyPath;

    // конструктор нельзя объявить как async
    // поэтому вынесено в отдельную функцию
    this.openDatabasePromise = this._openDatabase( keyPath );
  }

  async _openDatabase( keyPath: any ) 
  {
    try 
    {
      this.dbConnection = await openDatabasePromise( keyPath );
    } 
    catch ( error ) 
    {
      this.error = error;
      throw error;
    }
  }

  async _tx( txMode: any , callback: any ) 
  {
    await this.openDatabasePromise; // await db connection
    const transaction = this.dbConnection.transaction( [ OBJECT_STORE_NAME ], txMode );
    const objectStore = transaction.objectStore( OBJECT_STORE_NAME );
    return await callback( objectStore );
  }

  async findAll()
  {
    return this._tx( 'readonly', (objectStore: any) => getAllPromise( objectStore ) );
  }

  async findById( key: any )
  {
    return this._tx( 'readonly', (objectStore: any) => getPromise( objectStore, key ) );
  }

  async deleteById( key: any )
  {
    return this._tx( 'readwrite', (objectStore: any) => deletePromise( objectStore, key ) );
  }

  async save( item: any )
  {
    return this._tx( 'readwrite', (objectStore: any) => putPromise( objectStore, item ) );
  }

}