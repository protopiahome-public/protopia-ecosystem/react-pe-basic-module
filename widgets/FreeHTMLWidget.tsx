import React, { Component } from "react" 

type FreeHTMLWidgetProps = {
  container_class? : string
  html : string
}
class FreeHTMLWidget extends Component<FreeHTMLWidgetProps> {
  props:any
  render() {
    return (
      <div className={this.props.container_class} dangerouslySetInnerHTML={{ __html: this.props.html }} />
    )
  }
}
export default FreeHTMLWidget
