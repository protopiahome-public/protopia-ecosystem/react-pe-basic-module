import { vk_api_key } from "@/settings/config";
import { useEffect, useState } from "react"
import { AppToaster } from "../../../libs/useful";
import { Intent } from "@blueprintjs/core";

export default function useVKApi( _isDisabled:boolean = true ) : boolean {
    const [isSdkLoaded, setIsSdkLoaded] = useState<boolean>(false)
    const [isDisabled, setIsDisabled]   = useState<boolean>( _isDisabled )
    const addVK = () => {
        if (document.getElementById('vk-video-jssdk')) {
            setIsSdkLoaded(true)
            setIsDisabled(false)
            return
        } 
        setFbAsyncInit()
        loadSdkAsynchronously() 
    }
    const setFbAsyncInit = () => { 
        window.vkAsyncInit = () => 
            {
            try{
                if( vk_api_key() === "NONE") {
                    throw new Error('Vk API not connected by owner. Contact administrator');
                }
                window.VK.init({ apiId: vk_api_key() })
                loadSdkVideoAsynchronously()
                 
            }
            catch(e:any) {
                console.error(e.message)
                AppToaster.show({
                intent: Intent.DANGER,
                message: "VK API not loaded: " + e.message
                })
            }
        };
    }
    const loadSdkAsynchronously = () => {
        const el = document.createElement('script');
        el.type = 'text/javascript';
        el.src = 'https://vk.com/js/api/openapi.js?139';
        el.async = true;
        el.id = 'vk-jssdk';
        el.onload = function () { 
            //console.log("VK ready")
        } 
      document.getElementsByTagName('head')[0].appendChild(el);
    }
    
    const loadSdkVideoAsynchronously = () => {
        const el = document.createElement('script');
        el.type = 'text/javascript';
        el.src = 'https://vk.com/js/api/videoplayer.js?139';
        el.async = true;
        el.id = 'vk-video-jssdk';
        document.getElementsByTagName('head')[0].appendChild(el);
        el.onload = function () { 
            //console.log("player ready")
            setIsSdkLoaded(true)
            setIsDisabled(false)
        } 
      }
    useEffect(() => {
        addVK()
    }, [])
    return isDisabled
}