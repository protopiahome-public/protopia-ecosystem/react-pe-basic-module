import { ID } from "@/libs/interfaces/layouts"
import actions from "../data/actions"
import { USER_INFO_ACTION } from "../data/actionTypes"

const switchToBlog = (land_id: ID ) => { 
    actions(USER_INFO_ACTION, { landId: land_id })
}

export default switchToBlog